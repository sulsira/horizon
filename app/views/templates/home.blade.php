@section('header')
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{route('welcome')}}">Horizon 2020 The Gambia</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="{{route('welcome')}}">Home</a></li>
            <li><a href="{{route('funding')}}">Funding</a></li>
            <li><a href="{{route('participation')}}">Participation</a></li>
            <li><a href="{{route('ministry')}}">Ministry</a></li>
            <li><a href="{{route('research')}}">Research</a></li>
            <li><a href="{{route('contact')}}">Contact</a></li>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
@stop
