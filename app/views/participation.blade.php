@include('templates/home')
@section('content')
<div class="container">
      <div class="page-header">
        <h1>How to participate ?</h1>
      </div>
      <div class="page-content">
        <h3>Individual researchers: </h3>
        <p>
          the European Research Council and the Marie Sklodowska-Curie Actions grants offer opportunities for reseach and capacity building in Europe.
        </p>
        <h3>Collaboartive projects:  </h3>
        <p>
          consortia of three organisations from three countries working together on specific research areas are allocated most of the EU funding.
        </p>
      </div>
  </div>
@stop
@include('templates/admin-footer')