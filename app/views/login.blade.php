@include('templates/login')
@section('content')

<div class="col-xs-6 col-sm-3 placeholder">
  <div class="container">
    
      <h4>Sign in A.C.E Information system</h4>
      <hr>

    <form action="http://localhost/ace.app/public/login" method="post" class="form-signin" role="form">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="email" class="form-control" placeholder="Email address" required="" autofocus="">
        <input type="password" class="form-control" placeholder="Password" required="">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div>    

  </div>
@stop
@include('templates/admin-footer')