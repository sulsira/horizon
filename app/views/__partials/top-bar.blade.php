@section('header')
  		<nav>
		    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		      <div class="container-fluid">
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand" href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Admin</a>
		        </div>
		        <div class="navbar-collapse collapse ">
		          <ul class="nav navbar-nav navbar-right">
		            <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Surveys</a></li>
		            <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Supervisors</a></li>
		            <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Questionaires</a></li>
		            <li><a href="./Dashboard Template for Bootstrap_files/Dashboard Template for Bootstrap.htm">Candidates</a></li>
		            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Username <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings</a></li>
                <li><a href="#">Users</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Sessions</li>
                <li><a href="#">Logout</a></li>
              </ul>
            </li>
		          </ul>
		        </div>
		      </div>
		    </div>  			
  		</nav>
@stop