@include('templates/home')
@section('content')
<div class="container">
      <div class="page-header">
        <h1>Contact information </h1>
      </div>

      <div class="page-content">
        <h2>Support services</h2>
        <div class="address-like">
          <span> National Contact Point (NCP) </span> <br>
          <span>Ms Jainaba JAGNE</span><br>
          <span> Director of Research</span><br>
          <span>Directorate of Research</span><br>
          <abbr title="Ministry of Higher Education, Research, Science and Technology">MoHERST</abbr><br>
          <span>Futurelec Building </span><br>
          <span>Bertil Harding Highway</span><br>
          <span>Kotu</span><br>
            <em>Email:</em>  <span>jjagne@moherst.org</span>  <br>
          <span class="tel">Tel. no.: +220 9022209</span>
          <span class="website"> Website: </span> <a href="http://ec.europa.eu/programmes/horizon2020" target="_blank">horizon2020 programmes </a>
        </div>
        <hr>
<h2>Further information</h2>
<div>
  <a href="http://ec.europa.eu/research/participants/portal/desktop/en/home.html" target="_blank">Participant Portal</a> <br>
   <a href="http://cordis.europa.eu/home_en.html" target="_blank">CORDIS</a> <br>
   <a href="http://erc.europa.eu" target="_blank">European Research Council</a> <br>
   <a href="http://ec.europa.eu/research/mariecurieactions" target="_blank">Marie Sklodowska-Curie Actions</a> <br>
  
  
  
  
</div>


 



      </div>
  </div>
@stop
@include('templates/admin-footer')