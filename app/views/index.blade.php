@include('templates/home')
@section('content')
<div class="container">
      <div class="page-header">
        <h1>What is Horizon 2020? </h1>
      </div>
      <p class="lead">Horizon 2020 is the European Union’s Framework for Research and Innovation, running over a 7 year period from 2014 to 2020, with a budget of over €80 billion. Horizon 2020 is the biggest multinational research programe in the world. 
</p>
      <p>Horizon 2020 is open to the world i.e. researchers, universities, companies, non-governmental organisations, etc. regardless of place of residence or establishment are welcome to participate in research initiatives.</p>
      <div class="page-content">
        <h3>Objectives of Horizon 2020</h3>
        <ul type="square">
          <li>Excellent science </li>
          <li>Competitive industries </li>
          <li>Tackling global societal challenges</li>
        </ul>

        <div>
          <h3>Advantages of Horizon 2020 for international partners</h3>
          <ul>
            <li>
                <strong>Tackling global challenges together </strong>
                <p>
                  challenges that affect us all can only be solved at international level e.g. infectious diseases, energy security and climate change. Horizon 2020 provides the opportunity for economies of scale and expanded research scope shared across many nations.
                </p>
            </li>
            <li>
                <strong>Funding for excellent science </strong>
                <p>
                  researchers from anywhere in the world may be part of a consortia proposing collaborative research projects. Individual researchers anywhere in the world also have access to ERC and Marie Curie grants. The programme offers access to biggest budget yet for research & innovation.
                </p>
            </li>
            <li>
                <strong>  Focus on innovation </strong>
                <p>
the programme encourages innovators to move their ideas from the lab to the market.
                </p>
            </li>
            <li>
                <strong>  World class research infrastructures </strong>
                <p> the programme facilitates the development of global research infrastructures and the cooperation of European infrastructures with their non-European counterparts, ensuring global interoperability and access.
                </p>
            </li>
            <li>
                <strong>  Access to new networks and alliances  </strong>
                <p>
sharing of expertise and access to equipment, data and facilities and raising research profiles..
                </p>
            </li>
          </ul>
        </div>
      </div>
  </div>
@stop
@include('templates/admin-footer')