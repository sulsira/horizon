<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');
Route::get('welcome',array(
		'as' => 'welcome',
		'uses' => 'HomeController@showWelcome'
));
Route::get('login',array(
		'as' => 'login',
		'uses' => 'HomeController@showLogin'
));
Route::get('funding',array(
		'as' => 'funding',
		'uses' => 'HomeController@showFunding'
));
Route::get('participation',array(
		'as' => 'participation',
		'uses' => 'HomeController@showParticipation'
));
Route::get('ministry',array(
		'as' => 'ministry',
		'uses' => 'HomeController@showMinistry'
));
Route::get('research',array(
		'as' => 'research',
		'uses' => 'HomeController@showResearch'
));
Route::get('contact',array(
		'as' => 'contact',
		'uses' => 'HomeController@showContact'
));