<?php

class HomeController extends BaseController {

	protected $layout = 'layouts/default';
	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$this->layout->content = View::make('index');
	}
	
	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showLogin()
	{
		$this->layout->content = View::make('login');
	}
	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showFunding()
	{
		$this->layout->content = View::make('funding');
	}

	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showParticipation()
	{
		$this->layout->content = View::make('participation');
	}

	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showMinistry()
	{
		$this->layout->content = View::make('ministry');
	}	

	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showResearch()
	{
		$this->layout->content = View::make('research');
	}	

	/**
	 * Displave the login form pager.
	 *
	 * @return void 
	 */
	public function showContact()
	{
		$this->layout->content = View::make('contact');
	}

}
